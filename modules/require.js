function add (a, b) {
    return a + b;
}

function multiply(a, b) {
    return a * b;
}

//možnosti exportu

module.exports = {
    add: add,
    multiply: multiply
};

//v ES 6  stejné
module.exports = {
    add,
    multiply
};

//ES 6 má nové import/export nativně

export {
    add,
    multiply
};

export default function divide(a,b) { // je magie
    return a / b;
}