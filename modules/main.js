const {add, multiply} = require('./require');

//ES 6
import {add, multiply} from './require';

//import default
import funkce from './require'

// treeshaking webpack 4 , zkompiluje se jen potřebné metody v requiru, náročný na performance, jen v produkční kompilaci,

import * as calculator from './require'; // vytvoří objekt calculator s všemi property z knihovny , dobré pro řešení konfliktů
