//repo workshop-js-principles
//odkazy budou v mailu, odkazy poznamky
console.log('školení JS :D ---');

console.log(typeof null);
console.log(typeof 458.45);

console.log(Number.MAX_SAFE_INTEGER); // max číslo, s kterým JS umí bezpečně pracovat
console.log(0.1 + 0.2);

console.log(
    Math.PI,
    Number.NEGATIVE_INFINITY,
    Math.max(), // vrací -Infinity , protože funkce max hledá max číslo a začíná porovnávat od nejmenšího možného čísla
    0x100, // 256 - hexa zápis
    0o100, // binární zápis
);

//

var myObject = {}; // dvojite klíč hodnota , klíč muí být string (nebo symbol), honota libovolný string
var myObject2 = {
    aaa: {
        bbb: {
            ccc :'ddd'
        }
    }
};
console.log('složitější typy');
console.log(typeof myObject);

var index = 'ccc';

myObject2.ccc = 'ddd';

console.log(Object.keys(myObject2));
console.log(Object.values(myObject2));
console.log(Object.entries(myObject2)); //vrací klice s hodnotami jako pole

delete myObject.aaa;

//pole
var arrayItem = ['a',2,3,4,5,6];
console.log('pole')
console.log(typeof arrayItem);

//kontrola jestli položka je v poli
console.log(arrayItem.includes('3'));
// !!!daleko spolehlivější než metoda .indexOf() !== -1


console.log('set a map');
// nové datové typy
const mySet = new Set(['aaa','bbb','ccc','ttt', 'bbb']); // podobný jako object, ale obsahuje pouze unikátní hodnoty, duplicitní položky jsou ignorované
mySet.add('ff'); // přidání položky do setu

var mySet2 = new Set(['b', {'c': '123'}]);
console.log(typeof mySet);
console.log(mySet.has('b'));

const myMap = new Map(
    [
        ['aaa', 'bbb'],
        ['ccc', 'ddd']
    ]
);

//myMap.set('xxx','yyy');
//myMap.delete('aaa');
//myMap.clear(); // vyčístí celý map
console.log(typeof myMap);
console.log(myMap);

console.log('map foreach');
myMap.forEach(function(val, key) {
    console.log('index');
    console.log(key);
    console.log('value');
    console.log(val);
});

// cohersion
console.log('cohersion');
console.log('a' / 10);

// NaN === NaN
console.log(NaN !== NaN); // proč ?
var a = 'a' * 10; // NaN
var b = 'a' / 10; // NaN

// protože
varc = ('a' * 10 !==  'a' / 10);


//rozdíl mezi parseInt a parseFloat

var integers = ['1','2','3'];

console.log(integers.map(parseInt)); // map posílá tři argumenty do funkce, hodnota, funkce, originální honota
// ve skutečnosti se do parseInt dostane parseInt(hodnota, indexPole, originální hodnota)

//parseInt bere dva parametry, druhý parametr je číselná soustava do které chci string převést
console.log(parseInt('123',10));
console.log(parseInt('123',16)); // 256


//jediné bezpečé loose porovnání
function doSomething(param) {
    if(param == null) {
        // param není zadaný
    }
}

//scope

var aaa = 'xxx';

function doSomething() {
    var aaa = 'bbb';
    console.log(aaa); //bbbWWW
    function doSomethingElse() {
        //var aaa = 'ccc';
        console.log(aaa); // hledá proměnnou směrem nahoru ve scopech, nejbližší co  existuje
    }
    doSomethingElse();
}

console.log(aaa); //xxx

// let


let animal = 'dog';
if(true) {
    /*let */animal = 'cat';
    console.log(animal);
}
console.log(animal);
//-----------------

//"funkcionální","netypový"
// funkce jsou first class citizens , funkce se muze vratit z jiné funkce, s funkci muzu nakladat jakos kazdou jinou hodnout
// netypový , kdyz zakladam promennou nemusim definvat typ predem, muzu typ měnit v průběhu

//datové typy, scope, closure, context, asychronní kód

//ES 6

//typy
/* type of ===
Undefined	"undefined"
Null	"object" (see below)
Boolean	"boolean"
Number	"number"
BigInt (new in ECMAScript 2020)	"bigint"
String	"string"
Symbol (new in ECMAScript 2015)	"symbol"
Function object (implements [[Call]] in ECMA-262 terms)	"function"
Any other object	"object"
*/

//primitivní hodnoty vs. složitější typy

//primitivní null, string, symbol, number, boolean, undefined
/*
undefined = proměnná, která neexistuje,  existuje, ale nemá přiřazenou hodnotu
null = hodnota, která reprezentuje žádnou hodnotu .... null vs undefined , liší se tím, jak byly vytvořeny
 - anomálie !!! typeof null vrací object- v js je object fallback - = bug povýšení na feature , kdyby to opravili pulka vseho kodunebude fungovat

boolean = true / false
number = číslo, v jiých jazycich se rizlisuji vice typu cisel , v JS je jen jeden datový typ number (není float, interger)
 - v budoucnu přibude novýd atový typ bigint, za číslo n třeba 545454m
 - problém s malými precizními čísly, možna vznikne nový datový typ precision float s m nakonci, 0.03m
 - pokudchci pracovat s precizností , radši čísla násobit, třeba 1000, a pak vydělit
 - existuji iracionální číla, většinou v knihovně Math. například Math.PI
 - objekt Number.
 - NaN - je taky typ number, nereprezuntuje žádné číslo

stringy = libovolná posloupnost znaků, řetězec se definuje uvozovkami
 - uvozovky dvojiho typu, v jednoduchych uvozovkach lze psát dvojité 'toto je "testovací" text'
 - možno string pomoci backtics od ES6  `test string` - definování stringů úplně všude = zjednodušení
 - se stringy se dá pracovat jako s polem, můžu se dotázat na string 'text'[1] === 'e'
 - na string se dá použít metoda .length // anomálie ? console.log('emoji'.length) - modifikátory v emoji - vznikají delší stringy emoji - nekter emoji jsou kombinované z jinýchemoji


-----
složitější typy
object {}
 - JS interně převádí všechny klíče objektů na string . hodnoty v ojektech sedaji acesovat přestečkovou notaci nebo hranaté závorky
 - acesvání se může kombinovat tečková notace + hranaté závorky, do hranatých závorek muzu dat proměnnou -
 - pokud mám duplicitní index, použije se ten poslední
 - ! jak odebrat property  ,
 - pořadí položek v o objektu není garantované

pole [] - typeof array je také object !!!
 - v poli je pevně dané pořadí položek
 - v poli mám daleko více method, push, shift, pop
 - v poli je pořadí položek jasné na rozdíl od objektu

set - typeof object
 - hodí se pro seznam, který obsahuje pouze unikátní položky, například kombinace polí, vyhoí pouze unikátní hodnoty
 - není pevné pořadí, není zaručené
 - metody .add - přidání
 - set, klíč a hodnoty jsou v podstatě identické hodnoty

map - obsahuje také klíč a hodnotu
 - !!! klíč může mít jakoukoli hodnotu
 - muze mit funkci jako klíč


set a map mají identické rozhraní, metody: add, delete, add, .has = má hodnotu
 - stejné rozhraní pro iteraci , = forEach
 - set a map vhodné pro iteraci, iterované seznamy, dobré na výkon


cohersion : vynucení nějakého chování, operace
 -

 matematická operace
   - sčítání nad číslami a stringmy jsou jediné validní operace
   - jak JS postupuje při aritmetické funkci, která nedává smysl:
     'a' + 10 // 10 se převede na string , když je na levé straně string
     '10' * 5 // vrátí 50, JS se snaží být chytrý, máme operaci násobění, jaké hodnoty davájí smysl prí násobení, ok převedu první hodnotu na číslo , přes interní funkci která parsuje string na číslo
     '10a' * 5 // NaN - 10a není validní číslo
     '10' + 5 // vrátí string '105' , chová se jinak neý násobení, protože stringy se dají sčítat
      // převod na číslo Number.('123') = type number 123
      parseFloat , benevolentnější než Number('123.456.789') === NaN
      parseFloat('123.456.789') // vráti 123.456
      parseFloat('x123.456') === NaN
      - jde čísla zleva a kontroluje zda vpořádku


      parseInt('123') = 123
      parseInt('123.456') = 123

      rozdíl mezi parseInt a parseFloat


    další případy

    true * 5 // 5
    false * 5 // 0 , false se převede na nulu
    5 * [] // 0
    5 * [10] // 50 ,pole se na integer převádí na string a pak to zparsuje na číslo
    5 * [10, 20] // NaN , viz. níže
    //pole na string
    '' + [10,20] // string '10,20' , cohersion = automatické chování

    v domu v HTML, jsou všechny atributy string, i např. value u input type number

    // JS fuck

    +[]// 0 , plus vynutí sčítání
    +!![] === 1

    coherson se provádí i když se provádí loose porovnávání
    [] ==! [] // true
    // v loose porovnání můžu dostat šílené výsledky
    // !!! nikdy nepoužívat loose porovnání , jen v jednom případe null == undefined je bezpečné

scope :

 ve kterém nějaká proměnná existuje

 - tři tpy proměnných , var , let , const
 var :

 definice bez var:
 aaa = 'bbb' // automaticky se vytvoří v globálním scope, v node js je globální object process, v prohlížeči je globální object window , nemá žádného rodiče
 window.aaa // = bbb
 var - vytváří v daném scope, var funguje ve funkčním a globálním scope

 let - blokový scope , vše co je ve složených závorkách je blok

 nenibezpečnévšude vyměnit var za let například
 if(true) {
    let a = '1';
 }

 console.log(a); // udefined , u var ne

hoisting viz níže , převede všechny vary níže nahoru, let tak nefunguje,platnost proměnnélet je oddefinice níže,nedochází k hoisting
 - hoisting byl zavedeny aby se predeslo chybám

console.log(aaa);
var aaa = 'bbb';

hoisting pro funkce

doA(); // funguje , hoisting vytáhne funkci před její volání, funcguje u pojmenovaných funkcí
function doA()
{
    c.log('a')
}


pojmenovaná funkce viz. výše

anonymní funkce:
function() {
    console.log('jsem anonymní funkce');
}


// nefunguje
// error to hodí , protože se snažím vykonat undefined
doB(); // snažím se vykonat undefined
let doB = function() {
}

const , co se týče scope, je stejný jako let, const nemůžu běžit,

tedy
const a = 3;
a = 5; // error

let aaa = 'bbb';
let ccc = aaa;

aaa = 'xxx';

console.log(ccc); //bbb

nezmění se hodnota protože primitivní datové typy se nepřenášejí referencí, udělá se kopie,
pokud by bylo bbb object a změnil bych jeho nějakou hodnotu, tak se hodnota propíše



//-----

const defaultUser = {
    name: 'John',
    role: 'quest'
}

const me = defaultUser;
// mutuji me, tak mutuji i defaultUser !!!
// a převádím object


//jak vytvářet nové objekty z objektu
 - pomocí JSON.parse(JSON.stringify(object)); - nepřevede fukce,datumy ,cylické operace atd.
 - pomocí ručně udělané funkce
 - pomocí nějaké knihovny - například lodash.cloneDeep() // loadash je best library, optimalizováno na

 - pomocí Object.assign(); - je to shallow copy, vnořené reference zůstávají
 -
*/

function cloneObject(original) {
    //iterace přes objecty

}

//nebo Object assign
//const newObj = Object.assign({},defaultUser); // je toshallow copy //deep reference zůstávají

/*
*/

/*
--- closures ----

 */

function getPerson() {
    let name = 'Riki'
    return name;
}

console.log(getPerson()); // po povedení funkcina promennou namenevde pointer, vyhodí se z paměti

function getPerson() {
    let name = 'Riki'; //v tomto případě simulace privátní property
    return {
        getName: function() {
            return name;
        },
        setName: function(newName) {
            name = newName;
        }
    };
}
//closures:
const person = getPerson(); // dokud existuje tato proměnná,celý body funkce getPerson je v paměti, ikdyž funkce již proběhla a vrátila hodnotu - tomuse říkáclosure
console.log(person.getName());



/*
--- this ---
- odkazuje na aktuální instanci objektu



const person = {
    name: 'John',
    greet:function() {
        console.log('nazdar' + this.name);
    }
};
*/

const me = Object.create(person); // vytvoří instanci objektu z prototypu person,kdyžupravímperson upraví se instancovaný objekt me !!!


const john  = {
    name: 'Míra'
};

function greet(greeting, addition) {
    console.log(arguments); //seznam vše parametrů, které jsem poslal
    let output = 'c ' + this.name + ' ' + greeting + addition;
    console.log(output);
}

greet.call(john /*context*/, 'vole', '!');
//metoda apply -dela cokoli co call - maale 2 parametry
greet.apply(john,['borče',':D']);


// bind - nabinduje do funkce greet kontext objektu john

const greetMe = greet.bind(john);
greetMe();

// jeste reference
var arr1 = [];
var arr2 = [];
console.log(arr1 === arr2); // false , oba objekty maji jiné reference

/*
  --- ES6 ---
    - JS je jedna z implementaci EcmaScriptu , actionScript byl jednou z implementací taky
    - ES5 - nejdřív implementovaná  v IE 6, JS na více než 10 roků umřel na verzi 5
    - nové prohližeče se objevily a JS se taky začal vyvíjet, jakse začaly vyvíjet nové webové aplikaci,JS začal strádat , konsorcium začalo
    vyjíjet nové verze
    - 5 roků trvalo, než nové featury začaly prohližece podporovat
    - babel, webpack
    - každý rok vychází nováverze EcmaScriptu 2017,2018,2019

    // proposal na nové featury : https://github.com/tc39/proposals  - proposal úrovné 1 - 4
    - dlouho bylo ES5, ES6 přineso hodně změn

    - nové featury
    - classy
* */

class Person {
    constructor (name) { // zavolá se hned po instanci (new Class), je to klíčové slovo
        this.name = name;
        this._greeting = 'nazdar'; // je to privátní property
    }

    greet() {
        console.log(this._greeting + this.name);
    }

    //getery
    get greeting()
    {
        return this._greeting;
    }

    set greeting(newGreeting)
    {
        this._greeting = newGreeting.split('').reverse().join('');
    }
}

class Pirate extends Person { // dědění

    //když si vytvořim construktor pro subclassu, musímzavolat super()
    constructor(name) {
        super(name);
        this.name = name.toUpperCase();
    }
    greet() {
        //supermetody
        super.greet();
        console.log('Ahoy ' + this.name);
    }

}

const me = new Person('Míra');
me.greet();

const personPirate = new Pirate('John Long Silver');
personPirate.greeting('No čus');
console.log(personPirate.greeting);
personPirate.greet();

//--- můžu použvat .bind()

/*

--- fat arrow funkce ---
     - všechny fat arrow funkce jsou anonymní, nejsou pojmenované
() =>  {}
- vznikly protože chci většinou nabindovat funkce na aktuální kontext,nemusíme psát bind


 */
var result = [1,2,3,4,5].filter(function(n) {
        return n % 2 === 0
    }).map(function(n) {
        return n * n
    });

// nepřehledné

const isEvent = (n) => {
    return n % 2 === 0
}

const isEvent = n => n % 2 === 0;

//pomocí fat arrow
var result = [1,2,3,4,5]
    .filter(n => n % 2 === 0)
    .map(n => n * n);
console.log(result); //

/*
---ES6 backtics ---
 - backtikové stringy respektují newliny ,whitespacy
 */

function greetingGreet(greeting, person) {
    return `${greeting} no toto
     je text
     na dalším řádku ${person}`;
}
greetingGreet('čus','hováde');

/*

 - rest, spread atd..


- object
 */

const myData = {
    name : 'mira',
    surname: 'pech',
    middleName: 'x'
};

//chceme objekt rozebrat
const name = myData.name;
const surname = myData.surname;

// dekonstrukce objektu - to samé jako výše
const {name, surname} = myData;
//muzeme i takto
const {name: firstName,surname} = myData;

//defaultní hodnota
const {name: firstName,surname, middleName = 'y'} = myData;

//pole
//const [name, surname] = myData;


function greetPerson({
    firstName,
    lastName = '',
    greeting = 'Ahoj',
    title = ''
    } = {}) {

    console.log(`${greeting} ${title} ${firstName} ${lastName}`);
}

greetPerson({firstName:'Míra', greeting: 'Čus'});

// --- spread ---
// vezmeme pole nebo objekt a rozpostřeme ho
const myData = {
    fistName: 'M',
    lastName: 'o'
};

const  myOtherData = {
    greeting: 'ahoj',
    ...myData // doplníse všechy property  obj. myData
};

greetPerson({greeting:'no nazdar', ...myData});

//shallow clone

var newObj = {...myData};

//další příklad
const myData = [1,2,3];
const myOtherData = [8, ...myData]; // [8,1,2,3]

//

function greet(firstName, ...otherNames) { //spred muzu pouzit jen jako poslední parameter
    //other names je pole ['Van','Damm']
}

greet('Riki','Friedrich');
greet('Riki');
greet('Riki','Van', 'Damm');
//variabilni mnozstvi parametrů;


const userData = {
    fistName: 'M',
    lastName: 'o',
    middleName:'Lol'
};

//chci se zbavt middle name

const {middleName, ...userName} = userData;

console.log(userName); //objekt {fistName: 'M', lastName: 'o',}

/*
    !!!citát: čím sem starší, tím kod pisu hloupeji, jedno po druhem, prehledne, aby to bylo citelne a byl udrzitelnýkod prodruhe lidi, 1 řádkové "hacker" style zápisy = napiču
*/

/*
    --- ES6 import export
    viz. vedlejší soubor
*/


/*
    --- Asynchronní kód ---
*/

// async pattern / callbacky
/*
v asnyc kodu si s returny nevystačíme
* */


function returnWithDelay(delay, callback) {
    setTimeout(function() {
        callback(null,delay);
    }, delay * 1000);
}

returnWithDelay(1, console.log);
returnWithDelay(2, console.log);
returnWithDelay(3, console.log);

// u callbacku , je konvence, že pvní parametr je error, až druhý parameter je návratová honota

// callback hell//opakuje se if error else
// callback hell se da omezit tím, že jednotlivé zanoření převedu do samostatných funkcí -- flatten callback hell, pořád tam je handlování chyb

// Promise() - callbacky jsou schované v promisu v monádách --co jsou monády?
/*

*/

function getDelay(delay) {
    return  new Promise(function(resolve,reject) {
        if(typeof delay === 'number') {
            setTimeout(function() {
                console.log('uběhlo' + sekund + 'sekund');
                resolve(delay);
            },delay * 1000);
        } else {
            reject('není číslo');
        }

    });
}


getDelay(3)
    .then(function(result) {
        console.log(result);
    }).catch(function(error) { // volá se pokud je Promise rejectnutý
        console.log(error);
    });



//promisy se daji chainovat

getDelay
    .then(getDelay)
    .then(getDelay)
    .then(getDelay)
    .catch(function(error) {
        console.log('error')
    }).finally(() => console.log('provede se vždy'));

// - chainování then, do funkce v chainovaném then se pošle do funkce výsledek z předchozího resolve()
// pokud dám nakonec catch, tak je jedno v kterém then se vyskytne chyba, pokud se vyskytne chyba,další then se již nevkonají

/*
 - nová funkce .finnaly() - proběhne vždy, i když e vyskytl error
*/

// Promise.all

let myPromises= [
    getDelay(2),
    getDelay(3),
    getDelay(4),
    getDelay(6),
];

Promise.all(myPromises) // pole s funkcemi , spustí se všechny funkce najednou
    .then(function(allResults) {}) // pole výsledků až se všechy provedou
    .catch((e) => {console.log(e)}); // pokud jedna z prováděných se rejectne (error), zpracovní všech se přerusí

Promise.race().then(); // resolvne se po prvním resolve(), ne až po všech

//callbackové funkce jsou zastaralé, používají se už promisy, callbacky už nepoužívat


/*
    --- ES7 --- Async/Await - schované promisy aby to vypadalo jako lineální kód
 */

// když pred definici funkce dam slovo async,  funkce bude na pozadí promisová, když použiju await, tak to co je po nem, bude promis, a kod za await bude vykonaný až poslední await provede
// async await



function getNameDelayed(name) {  //musí vracet Promise

    //handle resolve
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve(name);
        }, 1000);


    });
}

async function handleName(name) {
    var result = await
}





